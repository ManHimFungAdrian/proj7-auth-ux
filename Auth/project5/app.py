import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import acp_times
import arrow

app = Flask(__name__)

client = MongoClient('db', 27017)
db = client.tododb

@app.route('/')
@app.route("/index")
def todo():
    app.logger.debug("Main page entry")
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('calc.html', items=items)

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route("/_onclick_submit")
def _onclick_submit():
    db.tododb.drop()

    app.logger.debug("Got a JSON request")
    km = request.args.get('km', type = float)
    open_time = request.args.get('open_time', type = str)
    close_time = request.args.get('close_time', type = str)


    item_doc = {"km": km, "Open time":open_time,"Close time": close_time}
    db.tododb.insert_one(item_doc)

    return flask.jsonify()

@app.route("/_onclick_display")
def _onclick_display():
    app.logger.debug("get data from mongodb")
    _items = db.tododb.find()
    items = [item for item in _items]
    for item in items:
        item.pop("_id")
    app.logger.debug(items)

    result = {"items": items}
    return flask.jsonify(result=result)


@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float) #control_dist_km

    brevet_distance = request.args.get('brev_dist', type = int)

    begin_dateandtime = request.args.get('begin_date',type=str) + ' ' + request.args.get('begin_time', type=str)+'-08:00'
    input_datetime = arrow.get(begin_dateandtime)

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km

    open_time = acp_times.open_time(km, brevet_distance, input_datetime.isoformat())
    close_time = acp_times.close_time(km, brevet_distance, input_datetime.isoformat())

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
