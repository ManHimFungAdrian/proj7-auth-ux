# Laptop Service
from flask import Flask
from flask_restful import Resource, Api
import os
import io
import csv
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import json
from bson import ObjectId



# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient('db', 27017)
db = client.tododb

class JSONEncoder(json.JSONEncoder):
    def default(self, o):	
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


class Laptop(Resource):
    def get(self):

    	return "Go http://0.0.0.0:5000/ for data in database.Go http://0.0.0.0:5000/listAll for data without id. go http://0.0.0.0:5000/listOpenOnly for open time "

        
class listAll(Resource):
	def get(self):
		app.logger.debug("get data from database")
		_items = db.tododb.find()
		if db.tododb.count() ==0:
			return "No Data in Database"
		items = [item for item in _items]
		for item in items:
			item.pop("_id")
		app.logger.debug(items)
		return JSONEncoder().encode(items)

class listOpenOnly(Resource):
	def get(self):
		app.logger.debug("get data from mongodb")
		_items = db.tododb.find()
		if db.tododb.count() ==0:
			return "No Data in Database"
		items = [item for item in _items]
		for item in items:
			item.pop("_id")
			item.pop("Close time")
		app.logger.debug(items)
		return JSONEncoder().encode(items)

class listCloseOnly(Resource):
	def get(self):
		app.logger.debug("get data from mongodb")
		_items = db.tododb.find()
		if db.tododb.count() ==0:
			return "No Data in Database"
		items = [item for item in _items]
		for item in items:
			item.pop("_id")
			item.pop("Open time")
		app.logger.debug(items)
		return JSONEncoder().encode(items)

class listAllcsv(Resource):
	def get(self):
		_items = db.tododb.find()
		if db.tododb.count() ==0:
			return "No Data in Database"
		items = [item for item in _items]
		for item in items:
			item.pop("_id")
		string =io.StringIO()
		writer = csv.writer(string, quoting=csv.QUOTE_ALL)
		writer.writerow(items)
		return string.getvalue()

class listOpencsv(Resource):
	def get(self):
		_items = db.tododb.find()
		if db.tododb.count() ==0:
			return "No Data in Database"
		items = [item for item in _items]
		for item in items:
			item.pop("_id")
			item.pop("Close time")
		string =io.StringIO()
		writer = csv.writer(string, quoting=csv.QUOTE_ALL)
		writer.writerow(items)
		return string.getvalue()

class listClosecsv(Resource):
	def get(self):
		_items = db.tododb.find()
		if db.tododb.count() ==0:
			return "No Data in Database"
		items = [item for item in _items]
		for item in items:
			item.pop("_id")
			item.pop("Open time")
		string =io.StringIO()
		writer = csv.writer(string, quoting=csv.QUOTE_ALL)
		writer.writerow(items)
		return string.getvalue()

# Create routes
# Another way, without decorators
api.add_resource(Laptop, '/')
api.add_resource(listAll,'/listAll','/listAll/json')
api.add_resource(listOpenOnly,'/listOpenOnly','/listOpenOnly/json')
api.add_resource(listCloseOnly,'/listCloseOnly','/listCloseOnly/json')

#CSV
api.add_resource(listAllcsv,'/listAll/csv')
api.add_resource(listOpencsv,'/listOpenOnly/csv')
api.add_resource(listClosecsv,'/listCloseOnly/csv')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
